## Menu Formatter

If you would like to render a menu in an entity reference field, this
is the module for you!

### Requirements

You must have the menu_ui and entity_reference core modules installed.

### Install

* Install as usual as per http://drupal.org/node/895232.

### Usage

* Create an entity reference field that references menu configuration
* Create content that references a menu. For example, the field could
use an autocomplete field to reference a menu.
* On the entity's display settings, set the display to use
"Rendered Menu".

### Maintainers

Current maintainers:
* George Anderson (geoanders) - https://www.drupal.org/u/geoanders

Past maintainers:
* David Lohmeyer (vilepickle) - https://www.drupal.org/u/vilepickle
