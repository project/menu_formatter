<?php

namespace Drupal\menu_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Menu\MenuLinkTree;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'menu_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "menu_field_formatter",
 *   label = @Translation("Rendered Menu"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class MenuFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The menu link tree.
   *
   * @var \Drupal\Core\Menu\MenuLinkTree
   */
  protected $menuLinkTree;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   The third party settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Menu\MenuLinkTree $menu_link_tree
   *   The menu link tree.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    AccountInterface $current_user,
    RendererInterface $renderer,
    MenuLinkTree $menu_link_tree
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->renderer = $renderer;
    $this->menuLinkTree = $menu_link_tree;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('renderer'),
      $container->get('menu.link_tree')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'menu_min_depth' => 1,
      'menu_max_depth' => 2,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['menu_min_depth'] = [
      '#type' => 'number',
      '#title' => $this->t('Menu Min Depth'),
      '#default_value' => $this->getSetting('menu_min_depth'),
      '#min' => 0,
    ];

    $form['menu_max_depth'] = [
      '#type' => 'number',
      '#title' => $this->t('Menu Max Depth'),
      '#default_value' => $this->getSetting('menu_max_depth'),
      '#min' => 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];

    if (!empty($this->getSetting('menu_min_depth'))) {
      $summary[] = $this->t('Menu min depth: %depth', ['%depth' => $this->getSetting('menu_min_depth')]);
    }

    if (!empty($this->getSetting('menu_max_depth'))) {
      $summary[] = $this->t('Menu max depth: %depth', ['%depth' => $this->getSetting('menu_max_depth')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    foreach ($items as $delta => $item) {
      $menu_name = $item->getValue()['target_id'];

      $menu_parameters = new MenuTreeParameters();
      if (!empty($this->getSetting('menu_min_depth'))) {
        $menu_parameters->setMinDepth($this->getSetting('menu_min_depth'));
      }
      if (!empty($this->getSetting('menu_max_depth'))) {
        $menu_parameters->setMaxDepth($this->getSetting('menu_max_depth'));
      }

      $tree = $this->menuLinkTree->load($menu_name, $menu_parameters);
      $manipulators = [
        ['callable' => 'menu.default_tree_manipulators:checkNodeAccess'],
        ['callable' => 'menu.default_tree_manipulators:checkAccess'],
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
      ];
      $tree = $this->menuLinkTree->transform($tree, $manipulators);

      $menu = $this->menuLinkTree->build($tree);
      $elements[$delta] = $menu;
    }

    return $elements;
  }

}
